package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Get all lists created by the user
//
// Returns a slice of lists
func (client *Gotodon) GetLists() ([]MList, error) {
	lists, err := gotodonr.GetLists(client.BaseUrl, client.AccessToken)
	var mlists []MList
	if err != nil {
		return mlists, err
	}

	for _, list := range lists {
		mlists = append(mlists, MList{list})
	}
	return mlists, nil
}

// Get a single list created by the user
//
// Returns a list
func (client *Gotodon) GetList(listId string) (MList, error) {
	list, err := gotodonr.GetList(client.BaseUrl, client.AccessToken, listId)
	return MList{list}, err
}

// Get all lists containing an account
//
// Returns a slice of lists containing the account
func (account *MAccount) GetLists(client *Gotodon) ([]MList, error) {
	lists, err := gotodonr.GetListsWithAccount(client.BaseUrl, client.AccessToken, account.Id)
	var mlists []MList
	if err != nil {
		return mlists, err
	}

	for _, list := range lists {
		mlists = append(mlists, MList{list})
	}
	return mlists, nil
}

// Gets a book used to scroll through the pages of accounts in a list
//
// Returns an AccountBook used to scroll through the pages or collect all at once
func (list *MList) GetAccounts(client *Gotodon) MAccountBook {
	return NewManagedAccountBook(gotodonr.ListAccountsBook(client.BaseUrl, client.AccessToken, list.Id, 0))
}

// Creates a new list
//
//	title: title of the new list
//
// Returns the newly created list
func (client *Gotodon) CreateList(title string) (MList, error) {
	list, err := gotodonr.CreateList(client.BaseUrl, client.AccessToken, title)
	return MList{list}, err
}

// Updates a list
//
//	 title: the new title of the list
//
// Returns the updated list
func (list *MList) Update(client *Gotodon, title string) (MList, error) {
	li, err := gotodonr.UpdateList(client.BaseUrl, client.AccessToken, list.Id, title)
	return MList{li}, err
}

// Deletes the list
//
//	listId: identifier of the list to be updated
func (list *MList) DeleteList(client *Gotodon) error {
	return gotodonr.DeleteList(client.BaseUrl, client.AccessToken, list.Id)
}

// Adds the accounts to the list
//
//	accounts: slice of accounts which should be added to the list
func (list *MList) AddAccounts(client *Gotodon, accounts ...string) error {
	return gotodonr.AddAccountsToList(client.BaseUrl, client.AccessToken, list.Id, accounts...)
}

// Removes the accounts from the list
//
//	accounts: slice of accounts which should be removed from the list
func (list *MList) RemoveAccounts(client *Gotodon, accounts ...string) error {
	return gotodonr.RemoveAccountsFromList(client.BaseUrl, client.AccessToken, list.Id, accounts...)
}
