package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book which can be used to scroll through pages of domains the user has blocked
//
// Returns a StringBook which can be used to scroll through the pages or collect all pages at once
func (client *Gotodon) GetDomainBlocksBook() gotodonr.StringBook {
	return gotodonr.GetDomainBlocksBook(client.BaseUrl, client.AccessToken, 0)
}

// Blocks the given domain (strip any protocols and such, domain needs to be 'domain.tld')
//
//	domain: the domain to be blocked
func (client *Gotodon) BlockDomain(domain string) error {
	return gotodonr.BlockDomain(client.BaseUrl, client.AccessToken, domain)
}

// Unblocks the given domain (strip any protocols and such, domain needs to be 'domain.tld')
//
//	domain: the domain to be unblocked
func (client *Gotodon) UnblockDomain(domain string) error {
	return gotodonr.UnblockDomain(client.BaseUrl, client.AccessToken, domain)
}
