package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book which can be used to scroll through pages of statuses favourited by the user
//
// Returns a StatusBook which can be used to scroll through the pages or collect all pages at once
func (client *Gotodon) GetFavourites() MStatusBook {
	return NewManagedStatusBook(gotodonr.GetFavouritesBook(client.BaseUrl, client.AccessToken, 0))
}

// Favourites the status
//
// Returns the status
func (status *MStatus) Favourite(client *Gotodon) (MStatus, error) {
	stat, err := gotodonr.FavouriteStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{stat}, err
}

// Unfavourites the status
//
//	statusId: identifier of the status which should be unfavourited
//
// Returns the status
func (status *MStatus) Unfavourite(client *Gotodon) (MStatus, error) {
	stat, err := gotodonr.UnfavouriteStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{stat}, err
}
