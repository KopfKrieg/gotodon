package gotodon

import "gitlab.com/KopfKrieg/gotodonr"

// Reports a user
//
//	statusIds: statuses which show violations of the ToS (optional)
//	  comment: additional information on why the user is being reported (optional, up to 1000 characters)
//	  forward: forward the report to the accounts server if it isn't the local server (default: false)
func (account *MAccount) Report(client *Gotodon, statusIds []string, comment string, forward bool) error {
	return gotodonr.Report(client.BaseUrl, client.AccessToken, account.Id, statusIds, comment, forward)
}
