package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Uploads a new media object which can be used as attachment
//
//	   filePath: path to the file which should be uploaded
//	description: description of the file (for screen readers and loading errors) (optional, max 420 characters))
//	     focusX: x-axis focal point, between 0.0 and 1.0 (default: 0.0)
//	     focusY: y-axis focal point, between 0.0 and 1.0 (default: 0.0)
//
// Returns the newly created attachment
func (client *Gotodon) UploadMedia(filePath, description string, focusX, focusY float64) (MAttachment, error) {
	att, err := gotodonr.UploadMedia(client.BaseUrl, client.AccessToken, filePath, description, focusX, focusY)
	return MAttachment{att}, err
}

// Updates an existing attachment
//
//	description: description of the file (for screen readers and loading errors) (optional, max 420 characters))
//	     focusX: x-axis focal point, between 0.0 and 1.0 (default: 0.0)
//	     focusY: y-axis focal point, between 0.0 and 1.0 (default: 0.0)
//
// Returns the updated attachment
func (attachment *MAttachment) UpdateMedia(client *Gotodon, description string, focusX, focusY float64) (MAttachment, error) {
	att, err := gotodonr.UpdateMedia(client.BaseUrl, client.AccessToken, attachment.Id, description, focusX, focusY)
	return MAttachment{att}, err
}
