package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Account, Status, Notification, Conversation

//region Managed Account Book

func transformAccountsArray(accounts []gotodonr.Account) []MAccount {
	var maccs []MAccount
	for _, acc := range accounts {
		maccs = append(maccs, MAccount{acc})
	}
	return maccs
}

// Creates a new ManagedAccountBook
func NewManagedAccountBook(accountBook gotodonr.AccountBook) MAccountBook {
	return MAccountBook{
		underlying: accountBook,
	}
}

// Book used to scroll through accounts
type MAccountBook struct {
	underlying gotodonr.AccountBook
}

// Returns the values from the current page
func (book *MAccountBook) Current() []MAccount {
	return transformAccountsArray(book.underlying.Current)
}

// Updates the book to the values from the previous page
func (book *MAccountBook) Previous() []MAccount {
	return transformAccountsArray(book.underlying.Previous())
}

// Updates the book to the values from the next page
func (book *MAccountBook) Next() []MAccount {
	return transformAccountsArray(book.underlying.Next())
}

// Returns all available accounts.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *MAccountBook) All() []MAccount {
	return transformAccountsArray(book.underlying.All())
}

// Updates the book using the values retrieved from the given url
func (book *MAccountBook) update(url string) []MAccount {
	return transformAccountsArray(book.underlying.Update(url))
}

// Retrieves a given page
func (book *MAccountBook) get(url, accessToken string) ([]MAccount, string, string, error) {
	accs, prev, next, err := book.underlying.Get(url, accessToken)
	return transformAccountsArray(accs), prev, next, err
}

//endregion
//region Managed Status Book

func transformStatusArray(statuses []gotodonr.Status) []MStatus {
	var mstats []MStatus
	for _, stat := range statuses {
		mstats = append(mstats, MStatus{stat})
	}
	return mstats
}

// Creates a new ManagedStatusBook
func NewManagedStatusBook(statusBook gotodonr.StatusBook) MStatusBook {
	return MStatusBook{
		underlying: statusBook,
	}
}

// Book used to scroll through accounts
type MStatusBook struct {
	underlying gotodonr.StatusBook
}

// Returns the values from the current page
func (book *MStatusBook) Current() []MStatus {
	return transformStatusArray(book.underlying.Current)
}

// Updates the book to the values from the previous page
func (book *MStatusBook) Previous() []MStatus {
	return transformStatusArray(book.underlying.Previous())
}

// Updates the book to the values from the next page
func (book *MStatusBook) Next() []MStatus {
	return transformStatusArray(book.underlying.Next())
}

// Returns all available accounts.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *MStatusBook) All() []MStatus {
	return transformStatusArray(book.underlying.All())
}

// Updates the book using the values retrieved from the given url
func (book *MStatusBook) update(url string) []MStatus {
	return transformStatusArray(book.underlying.Update(url))
}

// Retrieves a given page
func (book *MStatusBook) get(url, accessToken string) ([]MStatus, string, string, error) {
	accs, prev, next, err := book.underlying.Get(url, accessToken)
	return transformStatusArray(accs), prev, next, err
}

//endregion
//region Managed Notification Book

func transformNotificationsArray(notifications []gotodonr.Notification) []MNotification {
	var mnots []MNotification
	for _, not := range notifications {
		mnots = append(mnots, MNotification{not})
	}
	return mnots
}

// Creates a new ManagedNotificationBook
func NewManagedNotificationBook(notificationBook gotodonr.NotificationBook) MNotificationBook {
	return MNotificationBook{
		underlying: notificationBook,
	}
}

// Book used to scroll through accounts
type MNotificationBook struct {
	underlying gotodonr.NotificationBook
}

// Returns the values from the current page
func (book *MNotificationBook) Current() []MNotification {
	return transformNotificationsArray(book.underlying.Current)
}

// Updates the book to the values from the previous page
func (book *MNotificationBook) Previous() []MNotification {
	return transformNotificationsArray(book.underlying.Previous())
}

// Updates the book to the values from the next page
func (book *MNotificationBook) Next() []MNotification {
	return transformNotificationsArray(book.underlying.Next())
}

// Returns all available accounts.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *MNotificationBook) All() []MNotification {
	return transformNotificationsArray(book.underlying.All())
}

// Updates the book using the values retrieved from the given url
func (book *MNotificationBook) update(url string) []MNotification {
	return transformNotificationsArray(book.underlying.Update(url))
}

// Retrieves a given page
func (book *MNotificationBook) get(url, accessToken string) ([]MNotification, string, string, error) {
	accs, prev, next, err := book.underlying.Get(url, accessToken)
	return transformNotificationsArray(accs), prev, next, err
}

//endregion
//region Managed Account Book

func transformConversationsArray(conversations []gotodonr.Conversation) []MConversation {
	var mcons []MConversation
	for _, con := range conversations {
		mcons = append(mcons, MConversation{con})
	}
	return mcons
}

// Creates a new ManagedConversationBook
func NewManagedConversationBook(conversationBook gotodonr.ConversationBook) MConversationBook {
	return MConversationBook{
		underlying: conversationBook,
	}
}

// Book used to scroll through accounts
type MConversationBook struct {
	underlying gotodonr.ConversationBook
}

// Returns the values from the current page
func (book *MConversationBook) Current() []MConversation {
	return transformConversationsArray(book.underlying.Current)
}

// Updates the book to the values from the previous page
func (book *MConversationBook) Previous() []MConversation {
	return transformConversationsArray(book.underlying.Previous())
}

// Updates the book to the values from the next page
func (book *MConversationBook) Next() []MConversation {
	return transformConversationsArray(book.underlying.Next())
}

// Returns all available accounts.
// Warning: Can take a long time (receives a maximum of 80 accounts per second)
func (book *MConversationBook) All() []MConversation {
	return transformConversationsArray(book.underlying.All())
}

// Updates the book using the values retrieved from the given url
func (book *MConversationBook) update(url string) []MConversation {
	return transformConversationsArray(book.underlying.Update(url))
}

// Retrieves a given page
func (book *MConversationBook) get(url, accessToken string) ([]MConversation, string, string, error) {
	accs, prev, next, err := book.underlying.Get(url, accessToken)
	return transformConversationsArray(accs), prev, next, err
}

//endregion
