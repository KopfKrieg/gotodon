package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Get a servers custom emojis
//
// Returns a slice of emojis
func (client *Gotodon) GetCustomEmojis() ([]gotodonr.Emoji, error) {
	return gotodonr.GetCustomEmojis(client.BaseUrl)
}
