package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book used to scroll through the pages of statuses in the home timeline
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func (client *Gotodon) GetHomeTimeline() MStatusBook {
	return NewManagedStatusBook(gotodonr.GetHomeTimelineBook(client.BaseUrl, client.AccessToken, 0))
}

// Gets a book used to scroll through the pages of conversations
//
//	  limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a ConversationBook used to scroll through the pages or collect them all at once
func (client *Gotodon) GetConversations() MConversationBook {
	return NewManagedConversationBook(gotodonr.GetConversationsBook(client.BaseUrl, client.AccessToken, 0))
}

// Gets a book used to scroll through the pages of statuses in the public timeline
//
//      local: only retrieve local statuses (default: false)
//	onlyMedia: only retrieve statuses with attachments (default: false)
//	    limit: only retrieve this amount of statuses per page (default: 40)
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func (client *Gotodon) GetPublicTimeline(local, onlyMedia bool) MStatusBook {
	return NewManagedStatusBook(gotodonr.GetPublicTimelineBook(client.BaseUrl, client.AccessToken, local, onlyMedia, 0))
}

// Gets a book used to scroll through the pages of statuses in a hashtags timeline
//
//	  hashTag: the hashtag whose timeline to retrieve
//      local: only retrieve local statuses (default: false)
//	onlyMedia: only retrieve statuses with attachments (default: false)
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func (client *Gotodon) GetHashtagTimeline(hashTag string, local, onlyMedia bool) MStatusBook {
	return NewManagedStatusBook(gotodonr.GetHashtagTimelineBook(client.BaseUrl, client.AccessToken, hashTag, local, onlyMedia, 0))
}

// Gets a book used to scroll through the pages of statuses in a lists timeline
//
//	   listId: identifier of the list whose timeline to retrieve
//
// Returns a StatusBook used to scroll through the pages or collect them all at once
func (client *Gotodon) GetListTimeline(listId string) MStatusBook {
	return NewManagedStatusBook(gotodonr.GetListTimelineBook(client.BaseUrl, client.AccessToken, listId, 0))
}
