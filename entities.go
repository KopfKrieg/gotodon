package gotodon

import "gitlab.com/KopfKrieg/gotodonr"

// The app retrieved when registering a client at a mastodon server
type MApp struct {
	gotodonr.App
}

// A struct used when uploading media attachments within the status post call
type MMedia struct {
	gotodonr.Media
}

// An account of a user, with profile data and statistics
type MAccount struct {
	gotodonr.Account
}

// A field shown on a users profile (up to four fields can be set)
type MField struct {
	gotodonr.Field
}

// Source data of an account
type MSource struct {
	gotodonr.Source
}

// Token data retrieved when authorizing a client
type MToken struct {
	gotodonr.Token
}

// Display data of the application used to post a status
type MApplication struct {
	gotodonr.Application
}

// Data of an attachment which can be added to a status
type MAttachment struct {
	gotodonr.Attachment
}

// Metadata of an attachment
type MMeta struct {
	gotodonr.Meta
}

// Focus data of an image, used to align the preview image
type MFocus struct {
	gotodonr.Focus
}

// Metadata of an attachment
type MMetaAttributes struct {
	gotodonr.MetaAttributes
}

// Card data used to generate a preview/embedding for links in statuses
type MCard struct {
	gotodonr.Card
}

// The context of a status, used in conversations
type MContext struct {
	gotodonr.Context
}

// Data used for custom emojis
type MEmoji struct {
	gotodonr.Emoji
}

// Data sent by the server to provide information why an action/request failed
type MError struct {
	gotodonr.Error
}

// Data for a filter used to automatically hide statuses from users
type MFilter struct {
	gotodonr.Filter
}

// Data used to provide information about a mastodon server
type MInstance struct {
	gotodonr.Instance
}

// Data pointing to a endpoint which can be used by WebSockets
type MURLs struct {
	gotodonr.URLs
}

// Statistics for a mastodon server
type MStats struct {
	gotodonr.Stats
}

// Lists allow the creation of timelines which only contains statuses by a specified set of accounts
type MList struct {
	gotodonr.List
}

// Data of a mentioned account
type MMention struct {
	gotodonr.Mention
}

// Data of a notification
type MNotification struct {
	gotodonr.Notification
}

// Data of a poll
type MPoll struct {
	gotodonr.Poll
}

// Data for a single choice in a poll
type MPollOption struct {
	gotodonr.PollOption
}

// Data for the push subscription
type MPushSubscription struct {
	gotodonr.PushSubscription
}

// Set of alerts which a subscription can be subscribed to
// TODO: Should be booleans, but server returns JSON strings
type MAlerts struct {
	gotodonr.Alerts
}

// Relationship data between two accounts
type MRelationship struct {
	gotodonr.Relationship
}

// Set of search results
type MResult struct {
	gotodonr.Result
}

// Data of a status
type MStatus struct {
	gotodonr.Status
}

// Data of a scheduled status
type MScheduledStatus struct {
	gotodonr.ScheduledStatus
}

// Data of a status that is yet to be posted
type MStatusParams struct {
	gotodonr.StatusParams
}

// Data of a (hash) tag
type MTag struct {
	gotodonr.Tag
}

// A dated statistic of the uses for a (hash) tag
type MHistory struct {
	gotodonr.History
}

// Data of a conversation
type MConversation struct {
	gotodonr.Conversation
}
