package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Get all scheduled statuses
//
// Returns a slice of scheduled statuses
func (client *Gotodon) GetScheduledStatuses() ([]MScheduledStatus, error) {
	sstats, err := gotodonr.GetScheduledStatuses(client.BaseUrl, client.AccessToken)
	var msstats []MScheduledStatus
	if err != nil {
		return msstats, err
	}

	for _, sstat := range sstats {
		msstats = append(msstats, MScheduledStatus{sstat})
	}
	return msstats, nil
}

// Gets a scheduled status
//
// Returns the scheduled status
func (client *Gotodon) GetScheduledStatus(scheduledId string) (MScheduledStatus, error) {
	sstat, err := gotodonr.GetScheduledStatus(client.BaseUrl, client.AccessToken, scheduledId)
	return MScheduledStatus{sstat}, err
}

// Schedules a new status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func (client *Gotodon) ScheduleStatus(status, scheduledAt, inReplyToId string, mediaIds []string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MScheduledStatus, error) {
	sstat, err := gotodonr.ScheduleStatus(client.BaseUrl, client.AccessToken, status, scheduledAt, inReplyToId, mediaIds, pollOptions, pollExpiresIn, pollMultiple, pollHideTotals, sensitive, spoilerText, visibility, language, idempotencyKey)
	return MScheduledStatus{sstat}, err
}

// Schedules a new text status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func (client *Gotodon) ScheduleTextStatus(status, scheduledAt, inReplyToId string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MScheduledStatus, error) {
	return client.ScheduleStatus(status, scheduledAt, inReplyToId, nil, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Schedules a new media status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func (client *Gotodon) ScheduleMediaStatus(status, scheduledAt, inReplyToId string, mediaIds []string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MScheduledStatus, error) {
	return client.ScheduleStatus(status, scheduledAt, inReplyToId, mediaIds, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Uploads any attachments and schedules a new media status
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	        medias: slice of medias which should be uploaded and attached (optional)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func (client *Gotodon) UploadAndScheduleMediaStatus(status, scheduledAt, inReplyToId string, medias []gotodonr.Media, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MScheduledStatus, error) {
	var mediaIds []string
	for _, media := range medias {
		att, err := client.UploadMedia(media.Path, media.Description, media.FocusX, media.FocusY)
		if err != nil {
			return MScheduledStatus{}, err
		}
		mediaIds = append(mediaIds, att.Id)
	}
	return client.ScheduleMediaStatus(status, scheduledAt, inReplyToId, mediaIds, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Schedules a new poll
//
//	        status: the text of the status
//	   scheduledAt: timestamp of when the status will be posted
//	   inReplyToId: identifier of the status it will reply to (optional)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly scheduled status
func (client *Gotodon) SchedulePollStatus(status, scheduledAt, inReplyToId string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MScheduledStatus, error) {
	return client.ScheduleStatus(status, scheduledAt, inReplyToId, nil, pollOptions, pollExpiresIn, pollMultiple, pollHideTotals, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Moves a scheduled status to a new time
//
// 	scheduledAt: timestamp of when the status should be scheduled
//
// Returns the updated scheduled status
func (status *MScheduledStatus) Update(client *Gotodon, scheduledAt string) (MScheduledStatus, error) {
	sstat, err := gotodonr.UpdateScheduledStatus(client.BaseUrl, client.AccessToken, status.Id, scheduledAt)
	return MScheduledStatus{sstat}, err
}

// Deletes a scheduled status
func (status *MScheduledStatus) Delete(client *Gotodon) error {
	return gotodonr.DeleteScheduledStatus(client.BaseUrl, client.AccessToken, status.Id)
}
