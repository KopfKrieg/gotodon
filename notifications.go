package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book used to scroll through pages of notifications
//
//	excludeTypes: types to exclude (one or more of follow, favourite, reblog and/or mention)
//	   accountId: return notifications which were created due to actions performed by this account (optional)
//
// Returns a NotificationBook used to scroll through the pages or collect all at once
func (client *Gotodon) GetNotificationsBook(excludeTypes []string, accountId string) MNotificationBook {
	return NewManagedNotificationBook(gotodonr.GetNotificationsBook(client.BaseUrl, client.AccessToken, excludeTypes, accountId, 0))
}

// Get a notification
//
//	notificationId: identifier of the notification to get
//
// Returns the notifications
func (client *Gotodon) GetNotification(notificationId string) (MNotification, error) {
	not, err := gotodonr.GetNotification(client.BaseUrl, client.AccessToken, notificationId)
	return MNotification{not}, err
}

// Clear all notifications
func (client *Gotodon) ClearNotifications() error {
	return gotodonr.ClearNotifications(client.BaseUrl, client.AccessToken)
}

// Dismisses the notification
//
// Returns the notifications
func (notification *MNotification) Dismiss(client *Gotodon) error {
	return gotodonr.DismissNotification(client.BaseUrl, client.AccessToken, notification.Id)
}

// TODO: Subscriptions work so far (create, get, update, delete) but actually receiving them is untested

// Creates a new subscription
//
//	 endpoint: url of the push endpoint
//	   p256dh: public key (Base64 encoded string of public key of ECDH key using ‘prime256v1’ curve)
//	     auth: auth secret (Base64 encoded string of 16 bytes of random data)
//	   follow: receive follow notifications
//	favourite: receive favourite notifications
//	   reblog: receive reblog notifications
//	  mention: receive mention notifications
//	     poll: receive poll notifications
//
// Returns the newly created push subscription
func (client *Gotodon) CreateSubscription(endpoint, p256dh, auth string, follow, favourite, reblog, mention, poll bool) (MPushSubscription, error) {
	sub, err := gotodonr.CreateSubscription(client.BaseUrl, client.AccessToken, endpoint, p256dh, auth, follow, favourite, reblog, mention, poll)
	return MPushSubscription{sub}, err
}

// Gets the current subscription
func (client *Gotodon) GetSubscription() (MPushSubscription, error) {
	sub, err := gotodonr.GetSubscription(client.BaseUrl, client.AccessToken)
	return MPushSubscription{sub}, err
}

// Updates the subscription
//
//	   follow: receive follow notifications
//	favourite: receive favourite notifications
//	   reblog: receive reblog notifications
//	  mention: receive mention notifications
//	     poll: receive poll notifications
//
// Returns the updated push subscription
func (client *Gotodon) UpdateSubscription(follow, favourite, reblog, mention, poll bool) (MPushSubscription, error) {
	sub, err := gotodonr.UpdateSubscription(client.BaseUrl, client.AccessToken, follow, favourite, reblog, mention, poll)
	return MPushSubscription{sub}, err
}

// Deletes the current subscription
func (client *Gotodon) DeleteSubscription() error {
	return gotodonr.DeleteSubscription(client.BaseUrl, client.AccessToken)
}
