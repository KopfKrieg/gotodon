package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Search accounts, statuses and tags
//
//	query: query to look for
//	resolve: attempt a WebFinger lookup (default: false)
//	following: only include accounts the user is following (default: false)
//	limit: max number of results (default: 40)
// 	offset: result offset of the search (used for pagination) (default: 0)
//
// Returns a result, containing individual slices for accounts, statuses and tags
func (client *Gotodon) Search(query string, resolve, following bool, limit, offset int) (gotodonr.Result, error) {
	return gotodonr.Search(client.BaseUrl, client.AccessToken, query, resolve, following, limit, offset)
}
