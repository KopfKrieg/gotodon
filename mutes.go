package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book used to scroll through pages of muted accounts
//
// Returns an AccountBook used to scroll through the pages or collect them all at once
func (client *Gotodon) GetMutes() MAccountBook {
	return NewManagedAccountBook(gotodonr.GetMutesBook(client.BaseUrl, client.AccessToken, 0))
}

// Mutes the account
//
// notifications: if notifications for this account should be muted as well (default: true)
//
// Returns the updated relationship
func (account *MAccount) Mute(client *Gotodon, notifications bool) (gotodonr.Relationship, error) {
	return gotodonr.MuteAccount(client.BaseUrl, client.AccessToken, account.Id, notifications)
}

// Unmutes the account
//
// Returns the updated relationship
func (account *MAccount) Unmute(client *Gotodon) (gotodonr.Relationship, error) {
	return gotodonr.UnmuteAccount(client.BaseUrl, client.AccessToken, account.Id)
}

// Mutes the status
//
// Returns the status
func (status *MStatus) Mute(client *Gotodon) (MStatus, error) {
	stat, err := gotodonr.MuteStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{stat}, err
}

// Unmutes the status
//
// Returns the status
func (status *MStatus) Unmute(client *Gotodon) (MStatus, error) {
	stat, err := gotodonr.UnmuteStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{stat}, err
}
