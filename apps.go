package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Registers the client as an app
//
// 	 clientName: the name of the application (is shown on statuses)
//	    website: a website providing more information about your application (optional)
//
// Returns an error (if one occurs)
func (client *Gotodon) Register(clientname, website string) error {
	if clientname == "" {
		clientname = "Gotodon"
	}
	if website == "" {
		website = "https://gitlab.com/KopfKrieg/gotodon"
	}
	app, err := gotodonr.RegisterApp(client.BaseUrl, clientname, website, client.Scope, client.RedirectUri)
	if err != nil {
		return err
	}
	client.App = app
	return nil
}

// Returns an url at which a user can grant access to his account which will then return an AuthorizationCode.
//
// The AuthorizationCode can be used by AuthorizeWithAuthorizationCode to get an AccessToken.
func (client *Gotodon) GetAuthorizeUrl() string {
	return gotodonr.GetAuthorizeUrl(client.BaseUrl, client.ClientId, client.Scope, client.RedirectUri)
}

// Exchanges the AuthorizationCode to retrieve an AccessToken for this client
//
//	authorizationCode: the authorization code received from the user
//
// Updates the client with a new token
func (client *Gotodon) AuthorizeWithAuthorizationCode(authorizationCode string) error {
	token, err := gotodonr.GetAccessTokenFromAuthorizationCode(client.BaseUrl, client.ClientId, client.ClientSecret, authorizationCode, client.Scope, client.RedirectUri)
	if err != nil {
		return err
	}
	client.Token = token
	return nil
}

// Retrieves an AccessToken for this client by logging in with the login credentials. Does not store the username
// and password, which prevents the usage of AuthorizeWithLoginAgain() (this won't store the username and password
// which makes it safer than AuthorizeWithLogin, but it is encouraged to use the authorization code flow instead).
//
//     username: username to log in with
//     password: password to log in with
//
// Updates the client with a new token
func (client *Gotodon) AuthorizeWithLoginOnce(username, password string) error {
	token, err := gotodonr.GetAccessTokenFromLogin(client.BaseUrl, client.ClientId, client.ClientSecret, username, password, client.Scope, client.RedirectUri)
	if err != nil {
		return err
	}
	client.Token = token
	return nil
}

// Retrieves an AccessToken for this client by logging in with the login credentials. Stores the username
// and password, which allows the usage of AuthorizeWithLoginAgain() (storing the username and password is unsafe
// and it is encouraged to use the authorization code flow instead).
//
//     username: username to log in with
//     password: password to log in with
//
// Updates the client with a new token
func (client *Gotodon) AuthorizeWithLogin(username, password string) error {
	err := client.AuthorizeWithLoginOnce(username, password)
	if err == nil {
		client.Login = Login{
			Email:    username,
			Password: password,
		}
		return nil
	}
	return err
}

// Retrieves an AccessToken for this client by using AuthorizeWithLogin(email, password) using the stored username
// and password (storing the username and password is unsafe and it is encouraged to use the authorization code flow
// instead).
func (client *Gotodon) AuthorizeWithLoginAgain() error {
	return client.AuthorizeWithLogin(client.Email, client.Password)
}

// Gets a token by authorizing the application using its client credentials.
//
//	scope: space-separated list of scopes to be authorized for
//
// Updates the client with a new token
//
// Note: the token received from this method is an application token, not an account token. It can be used for functions
// which require an authentication, but will fail for all functions requiring a user (like posting a status, ...).
// After authorizing this way, use CreateAccount(...) which creates an account which can be used for all those
// functions (but will fail if the server has account registration turned off).
func (client *Gotodon) AuthorizeWithClientCredentials() error {
	token, err := gotodonr.GetAccessTokenFromCredentials(client.BaseUrl, client.ClientId, client.ClientSecret, client.Scope)
	if err != nil {
		return err
	}
	client.Token = token
	return nil
}

// Verifies that the AccessToken is at least a valid client token (contrary to an access token associated with an
// account). AccessTokens that are valid can be used to authorize the client but unless they were received
// using an authorization method, they can't be used to post statuses, update a profile or use other methods that
// require an account.
//
// See tootsuite/mastodon/app/controllers/api/v1/apps/credentials_controller.rb:
// https://github.com/tootsuite/mastodon/blob/master/app/controllers/api/v1/apps/credentials_controller.rb
func (client *Gotodon) VerifyAccessToken() error {
	_, err := gotodonr.VerifyCredentials(client.BaseUrl, client.AccessToken)
	if err != nil {
		return err
	}
	return nil
}

// Revoke this clients AccessToken (all authorized calls will fail until the client is authorized again)
func (client *Gotodon) RevokeAccessToken() error {
	err := gotodonr.RevokeToken(client.BaseUrl, client.ClientId, client.ClientSecret, client.AccessToken)
	if err != nil {
		return err
	}
	return nil
}

// Check if the AccessToken is invalid, and if so, get a new one (only possible when using Login as authorization method)
func (client *Gotodon) TokenMaintenance() error {
	err := client.VerifyAccessToken()
	if err != nil {
		return client.AuthorizeWithLoginAgain()
	}
	return nil
}
