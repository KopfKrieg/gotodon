package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a poll
//
//	pollId:  identifier of the poll to get
//
// Returns the poll
func (client *Gotodon) GetPoll(pollId string) (MPoll, error) {
	poll, err := gotodonr.GetPoll(client.BaseUrl, pollId)
	return MPoll{poll}, err
}

// Posts a vote on the poll
//
//	choices: one (or more) ordinals of choices to vote on (multiple ordinals can only be used when the vote allows multiple choices)
//
// Returns the updated poll
func (poll *MPoll) Vote(client *Gotodon, choices ...int) (MPoll, error) {
	p, err := gotodonr.VoteOnPoll(client.BaseUrl, client.AccessToken, poll.Id, choices...)
	return MPoll{p}, err
}
