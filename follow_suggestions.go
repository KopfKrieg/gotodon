package gotodon

import "gitlab.com/KopfKrieg/gotodonr"

// Gets all follow suggestions
//
// Returns a slice of suggested accounts
func (client *Gotodon) GetFollowSuggestions() ([]MAccount, error) {
	accs, err := gotodonr.GetFollowSuggestions(client.BaseUrl, client.AccessToken)
	var maccs []MAccount
	if err != nil {
		return maccs, err
	}

	for _, acc := range accs {
		maccs = append(maccs, MAccount{acc})
	}
	return maccs, nil
}

// Deletes a follow suggestion
func (account *MAccount) DeleteFollowSuggestion(client *Gotodon) error {
	return gotodonr.DeleteFollowSuggestion(client.BaseUrl, client.AccessToken, account.Id)
}
