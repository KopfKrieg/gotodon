package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book which can be used to scroll through pages of open follower requests
//
// Returns an AccountBook which can be used to scroll through the pages or collect all pages at once
func (client *Gotodon) GetFollowRequests() MAccountBook {
	return NewManagedAccountBook(gotodonr.GetFollowRequestsBook(client.BaseUrl, client.AccessToken, 0))
}

// Accepts a follower request
//
//	accountId: identifier of the account whose follow request should be accepted
func (account *MAccount) AuthorizeFollowRequest(client *Gotodon) error {
	return gotodonr.AuthorizeFollowRequest(client.BaseUrl, client.AccessToken, account.Id)
}

// Rejects a follower request
//
//	accountId: identifier of the account whose follow request should be rejected
func (account *MAccount) RejectFollowRequest(client *Gotodon) error {
	return gotodonr.RejectFollowRequest(client.BaseUrl, client.AccessToken, account.Id)
}
