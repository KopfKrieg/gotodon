package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Get all currently setup filters
//
// Returns a slice of filters
func (client *Gotodon) GetFilters() ([]MFilter, error) {
	filters, err := gotodonr.GetFilters(client.BaseUrl, client.AccessToken)
	var mfilters []MFilter
	if err != nil {
		return mfilters, err
	}

	for _, filter := range filters {
		mfilters = append(mfilters, MFilter{filter})
	}
	return mfilters, nil
}

// Get a specific filter
//
//	filterId: identifier of the filter to be retrieved
//
// Returns the filter for the given id
func (client *Gotodon) GetFilter(filterId string) (MFilter, error) {
	filter, err := gotodonr.GetFilter(client.BaseUrl, client.AccessToken, filterId)
	return MFilter{filter}, err
}

// Creates a new filter
//
//	      phrase: the phrase to be checked for in statuses
//	     context: the context(s) in which the filter should be applied (one or more of home, notifications, public and/or thread)
//	irreversible: if true, matching statuses will be dropped from the timeline instead of hidden (only works with context home and notifications) (default: false)
//	   wholeWord: whether to consider word boundaries when matching (default: false)
//	   expiresIn: time in seconds when the filter will expire or no-expiration when set to 0 (default: 0)
//
// Returns the newly created filter
func (client *Gotodon) CreateFilter(phrase string, context []string, irreversible, wholeWord bool, expiresIn int) (MFilter, error) {
	filter, err := gotodonr.CreateFilter(client.BaseUrl, client.AccessToken, phrase, context, irreversible, wholeWord, expiresIn)
	return MFilter{filter}, err
}

// Updates an existing filter
//
//	      phrase: the phrase to be checked for in statuses
//	     context: the context(s) in which the filter should be applied (one or more of home, notifications, public and/or thread)
//	irreversible: if true, matching statuses will be dropped from the timeline instead of hidden (only works with context home and notifications) (default: false)
//	   wholeWord: whether to consider word boundaries when matching (default: false)
//	   expiresIn: time in seconds when the filter will expire or no-expiration when set to 0 (default: 0)
//
// Returns the updated filter
func (filter *MFilter) UpdateFilter(client *Gotodon, filterId, phrase string, context []string, irreversible, wholeWord bool, expiresIn int) (MFilter, error) {
	fil, err := gotodonr.UpdateFilter(client.BaseUrl, client.AccessToken, filter.Id, phrase, context, irreversible, wholeWord, expiresIn)
	return MFilter{fil}, err
}

// Removes a filter
func (filter *MFilter) DeleteFilter(client *Gotodon) error {
	return gotodonr.DeleteFilter(client.BaseUrl, client.AccessToken, filter.Id)
}
