package gotodon

import (
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
)

// OpenUrl opens the given url in the users default browser.
//
// Kudos to icza (https://stackoverflow.com/a/39324149)
func OpenUrl(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}

	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
	}

	args = append(args, url)
	return exec.Command(cmd, strings.Join(args, " ")).Start()
}

// Returns the path to the directory the go app is executed from
func GetLocalPath() (string, error) {
	return filepath.Abs(filepath.Dir(os.Args[0]))
}
