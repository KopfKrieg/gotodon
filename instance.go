package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets information about a server
//
// Returns an instance
func (client *Gotodon) GetInstance() (gotodonr.Instance, error) {
	return gotodonr.GetInstance(client.BaseUrl)
}
