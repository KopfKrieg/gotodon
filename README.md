# Gotodon - Managed

Gotodon uses GotodonREST but adds some management to it. Mainly it reduces the amount of repetition
you have to do (by writing baseUrl and token.AccessToken) while also providing an object-oriented approach (functions
which operate on an object, like lets say liking a status, are performed on a status: ```someStatus.Favourite(client)``` )

All repositories which Gotodon consists of:
- [Gotodon REST](https://gitlab.com/Cyphrags/gotodonr)
- [Gotodon](https://gitlab.com/Cyphrags/gotodon)
- [Gotodon WebSocket](https://gitlab.com/Cyphrags/gotodonws)
- [Gotodon Examples](https://gitlab.com/Cyphrags/gotodon-examples)

# Features
- [OAuth](https://docs.joinmastodon.org/api/authentication/)
    - Authorization Code Flow
    - Password Grant Flow
    - Client Credentials Flow
    - Verifying Application/Access Tokens
    - Revoking Access Tokens
- All API endpoints as of July 2019

# Getting Started
This section will cover everything necessary to get started, from registering a client to getting an access token 
using any of the three available flows (Authorization Code Flow, Password Grant Flow and Client Credentials Flow)

We start off by declaring constants and globals which will be frequently used in api calls.

```go
const (
    // The address which points to the mastodon instance you want to access
    baseUrl = "domain.tld"
)

// The client which holds any data necessary (combines App and Token)
var client Gotodon

```

## Instantiating the client

First we create an instance of the Gotodon client.

```go
client = CreateClient(

    // The baseUrl, as stated above, is the address of the server to connect to
    baseUrl,

    // These are the scopes we want the client to request when authorizing
    Scopes(ScopeRead, ScopeWrite, ScopeFollow, ScopePush),

    // Here you can set a redirect uri, which will be used when retrieving authorization codes or access token
    "",

    // Filepath is used when storing the client
    "")
``` 

## Registering the client

After we created the instance, we need to register it as an app.

```go
err = client.Register(

    // The clientName is displayed on statuses on the bottom right (when the status is opened)
    "Gotodon",

    // If a website is set, a user can click on the clientName in a status and it will take him to it.
    "https://gitlab.com/Cyphrags/gotodon-examples/blob/master/managed_example.go")
```

## Authorizing the client

Now that we have registered the app, we need to authorize it. This will grant us an [Access Token](https://www.oauth.com/oauth2-servers/access-tokens/)
which will tells the server who is calling and which account the call will effect (i.e. who is posting the status or 
which profile to update with this nice, new avatar image).

There are three ways to authorize your client:
 1. [Authorization Code Flow (ACF)](https://gitlab.com/Cyphrags/gotodon#authorization-code-flow)
 2. [Password Grant Flow (PGF)](https://gitlab.com/Cyphrags/gotodon#password-grant-flow)
 3. [Client Credentials Flow (CCF)](https://gitlab.com/Cyphrags/gotodon#client-credentials-flow)
 
ACF and PGF require that an account already exists. Additionally, ACF requires the user to confirm the app (which 
removes the to pass any password around which makes it ultimately safer). CCF will create a new account.

> These might not be official/commonly used abbreviations. I used them here for simplicities sake. 

### Scopes

All authorization methods require a set of scopes which will restrict which functions the client can call. You can 
find a list of scopes in [gotodonr/constants.go](https://gitlab.com/Cyphrags/gotodonr/blob/master/constants.go) 
(and other constants too, like Visibility for statuses).
> You should only request the scopes you really need rather than requesting everything. Doing so can reduce
> the damage caused if an access token is leaked or stolen.
 
### Authorization Code Flow

In the [Authorization Code Flow](https://oauth.net/2/grant-types/authorization-code/) we first generate an URL which the user has to open (this website is the OAuth web
endpoint of the mastodon server). The user then needs to confirm that he wants to authorize the app which will 
present him with an authorization code (using the redirect uri and a local webserver, the authorization code can
also be retrieved automatically after the user hit confirm, instead of having them enter it somewhere).

```go
authorizeUrl := client.GetAuthorizeUrl()

// This placeholder variable needs to be filled with the authorization code and how that happens is up to you.
var authorizationCode string
```

After the user confirmed the authorization request and the authorization code was entered/retrieved, we simply 
exchange it for an access token (the scope used in ```GetAuthorizeUrl()``` and here need to be the same):

```go
err = client.AuthorizeWithAuthorizationCode(authorizationCode)
```

### Password Grant Flow

In the [Password Grant Flow](https://oauth.net/2/grant-types/password/) we use a login-style flow to get an access token, which means that we simply send 
the username and password to the server to get an access token. 

> Requiring a user to login is a potential security risk and/or might scare off some users. In any case, the 
>Authorization Code Flow is preferable (and I strongly advice you to use it).

```go
err = client.AuthorizeWithLoginOnce(

			// The username used to login (usually the e-mail address).
			// How you get the username is up to you (could be hardcoded, through environment variables or by
			// asking the user through a (web-)interface, popup, console, ...)
			username,

			// The password used to login.
			// How you get the password is up to you (could be hardcoded, through environment variables or by
			// asking the user through a (web-)interface, popup, console, ...)
			password)
```

``client`` provides three ```AuthorizeWithLogin*``` methods:
 - ``AuthorizeWithLogin`` which retrieves an access token and stores the username and password
 - ``AuthorizeWithLoginAgain`` which retrieves an access token if the client has a username and password stored
 - ``AuthorizeWithLoginOnce`` which retrieves an access token but doesn't store the username and password

While storing the username and password can be convenient, I strongly advice against it since both are stored as
plain text (so please use AuthorizeWithLoginOnce over AuthorizeWithLogin, or use the Authorization Code Flow instead).

### Client Credentials Flow

In the [Client Credentials Flow](https://oauth.net/2/grant-types/client-credentials/) we first get a special access token which we then use to create a new account
and then get an actual access token for the account we created.

```go
err = client.AuthorizeWithClientCredentials()
```

> This token is not linked to an account and can't be used in any account related methods. 

Now we use this token to create a new account.

```go
err = client.CreateAccount(

    // The UserName which is used to "@" this account (@UserName@domain.tld)
    // This is not the displayName, which can always be changed.
    // The userName, however, can only be changed by moving the account.
    username,

    // The email address used for this account. It will receive the initial "confirm your account" email.
    email,

    // The password used to login (in case the client needs to login again or the user wants to login
    // through the web interface, app or another client).
    password,

    // Basically needs to be true and confirms that you agree with the terms and conditions.
    true,

    // The locale which affects the "confirm your account" email
    "en")
```

> ``CreateAccount`` might throw an "you can't access this page" error if user registration is disabled

### Revoking an Access Token

If you ever need to revoke an access token (i.e. the client is deleted/uninstalled or gets a brand new token) you can
simply use the function ```RevokeToken```:

```go
err = client.RevokeAccessToken()
```

## Verifying the client

Now that we have setup the client, we can finally do something with it. To test that everything went alright,
we might simply post a status which will tell the world that everything is fine:

```go
client.PostTextStatus("Setup successful", "", false, "", VisibilityPublic, "en", "")
```

## Loading and storing the client

Once a client is created, it can be simply stored to use it again at a later time or after a restart.

```go
err = client.Store(storagePath)
```

If you want to store the client yourself (i.e. to store it encrypted or with other configuration files), the client
can be also be serialized and retrieved as a string.

```go
err, serializedClient = client.ToString()
```

Now we can load the client again if we need it.

```go
err, client = LoadClient(storagePath)
```

If you chose to store it yourself, you only need to deserialize it.

```go
err, client = LoadClientFromString(serializedClient)
```

It is also possible to create a client from an access token. However, this client is incomplete (lacks certain data like
 the ClientId and ClientSecret) but is enough for normal applications (it can't be used to get an access token though).

```go
err, client = LoadClientFromAccessToken(baseUrl, "ReplaceThisWithAnAccessToken")
```

> ``storagePath`` is the full path including the file name, i.e. '/home' would try to create a file 'home' in your root 
> directory. Instead it should read '/home/client.json'.

> You can view the full example here: [gotodon-examples/managed_example.go](https://gitlab.com/Cyphrags/gotodon-examples/blob/master/managed_example.go)