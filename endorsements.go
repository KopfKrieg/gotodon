package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book which can be used to scroll through pages of accounts the user has endorsed
// (endorsed accounts are highlighted on the users profile)
//
// Returns an AccountBook which can be used to scroll through the pages or collect all pages at once
func (client *Gotodon) GetEndorsements() MAccountBook {
	return NewManagedAccountBook(gotodonr.GetEndorsementsBook(client.BaseUrl, client.AccessToken, 0))
}

// Endorses (pins) the account
//
// Returns the updated relationship
func (account *MAccount) Endorse(client *Gotodon) (gotodonr.Relationship, error) {
	return gotodonr.EndorseAccount(client.BaseUrl, client.AccessToken, account.Id)
}

// Unendorses (unpins) the account
//
// Returns the updated relationship
func (account *MAccount) Unendorse(client *Gotodon) (gotodonr.Relationship, error) {
	return gotodonr.UnendorseAccount(client.BaseUrl, client.AccessToken, account.Id)
}
