package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a status
//
//	statusId: identifier of the status to get
//
// Returns the status
func (client *Gotodon) GetStatus(statusId string) (MStatus, error) {
	stat, err := gotodonr.GetStatus(client.BaseUrl, statusId)
	return MStatus{stat}, err
}

// Gets the context of the status
//
// Returns the context
func (status *MStatus) GetContext(client *Gotodon) (gotodonr.Context, error) {
	return gotodonr.GetStatusContext(client.BaseUrl, status.Id)
}

// Gets the card of the status
//
// Returns the card
func (status *MStatus) GetCard(client *Gotodon) (gotodonr.Card, error) {
	return gotodonr.GetStatusCard(client.BaseUrl, status.Id)
}

// Gets a book used to scroll through the pages of accounts which reblogged the status
//
// Returns an AccountBook which can be used to scroll through the pages or collect all at once
func (status *MStatus) GetRebloggedBy(client *Gotodon) MAccountBook {
	return NewManagedAccountBook(gotodonr.GetStatusRebloggedByBook(client.BaseUrl, status.Id, 0))
}

// Gets a book used to scroll through the pages of accounts which favourited the status
//
// Returns an AccountBook which can be used to scroll through the pages or collect all at once
func (status *MStatus) GetFavouritedBy(client *Gotodon) MAccountBook {
	return NewManagedAccountBook(gotodonr.GetStatusFavouritedByBook(client.BaseUrl, status.Id, 0))
}

// Posts a new status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func (client *Gotodon) PostStatus(status, inReplyToId string, mediaIds []string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MStatus, error) {
	sta, err := gotodonr.PostStatus(client.BaseUrl, client.AccessToken, status, inReplyToId, mediaIds, pollOptions, pollExpiresIn, pollMultiple, pollHideTotals, sensitive, spoilerText, visibility, language, idempotencyKey)
	return MStatus{sta}, err
}

// Posts a new text status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func (client *Gotodon) PostTextStatus(status, inReplyToId string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MStatus, error) {
	return client.PostStatus(status, inReplyToId, nil, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Posts a new media status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func (client *Gotodon) PostMediaStatus(status, inReplyToId string, mediaIds []string, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MStatus, error) {
	return client.PostStatus(status, inReplyToId, mediaIds, nil, 0, false, false, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Uploads any attachments and posts a new media status
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	      mediaIds: identifiers of attachments (media status)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func (client *Gotodon) UploadAndPostMediaStatus(status, inReplyToId string, medias []gotodonr.Media, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MStatus, error) {
	var mediaIds []string
	for _, media := range medias {
		att, err := client.UploadMedia(media.Path, media.Description, media.FocusX, media.FocusY)
		if err != nil {
			return MStatus{}, err
		}
		mediaIds = append(mediaIds, att.Id)
	}
	return client.PostMediaStatus(status, inReplyToId, mediaIds, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Posts a new poll
//
//	        status: the text of the status
//	   inReplyToId: identifier of the status it will reply to (optional)
//	   pollOptions: options for the poll (poll status)
//	 pollExpiresIn: time in seconds the poll will be live (poll status) (default: DurationDay)
// 	  pollMultiple: set to true to allow multiple choices (poll status) (default: false)
// 	pollHideTotals: set to true to hide the results while the poll is live (poll status) (default: false)
// 	     sensitive: set to true if the post contains sensitive content (default: false)
//	   spoilerText: text which will be shown instead if the status is marked as sensitive (optional)
//	    visibility: visibility of the status (either public, private, unlisted or direct) (default: public)
//	      language: the language of the status (ISO 8601) (optional)
//	idempotencyKey: helps preventing duplicate posts (optional)
//
// Returns the newly posted status
func (client *Gotodon) PostPollStatus(baseUrl, accessToken, status, inReplyToId string, pollOptions []string, pollExpiresIn int, pollMultiple, pollHideTotals, sensitive bool, spoilerText, visibility, language, idempotencyKey string) (MStatus, error) {
	return client.PostStatus(status, inReplyToId, nil, pollOptions, pollExpiresIn, pollMultiple, pollHideTotals, sensitive, spoilerText, visibility, language, idempotencyKey)
}

// Deletes the status
func (status *MStatus) Delete(client *Gotodon) error {
	return gotodonr.DeleteStatus(client.BaseUrl, client.AccessToken, status.Id)
}

// Reblogs the status
func (status *MStatus) Reblog(client *Gotodon) (MStatus, error) {
	sta, err := gotodonr.ReblogStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{sta}, err
}

// Unreblogs the status
func (status *MStatus) Unreblog(client *Gotodon) (MStatus, error) {
	sta, err := gotodonr.UnreblogStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{sta}, err
}

// Pins the status
func (status *MStatus) Pin(client *Gotodon) (MStatus, error) {
	sta, err := gotodonr.PinStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{sta}, err
}

// Unpins the status
func (status *MStatus) Unpin(client *Gotodon) (MStatus, error) {
	sta, err := gotodonr.UnpinStatus(client.BaseUrl, client.AccessToken, status.Id)
	return MStatus{sta}, err
}
