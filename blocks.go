package gotodon

import (
	"gitlab.com/KopfKrieg/gotodonr"
)

// Gets a book of accounts the user has blocked.
//
// Returns an AccountBook which can be used to scroll through the pages of blocked accounts.
func (client *Gotodon) GetAccountBlocks() MAccountBook {
	return NewManagedAccountBook(gotodonr.GetAccountBlocksBook(client.BaseUrl, client.AccessToken, 0))
}

// Blocks the account.
//
// Returns the updated relationship
func (account *MAccount) BlockAccount(client *Gotodon) (MRelationship, error) {
	rel, err := gotodonr.BlockAccount(client.BaseUrl, client.AccessToken, account.Id)
	return MRelationship{rel}, err
}

// Unblocks the account
//
// Returns the updated relationship
func (account *MAccount) UnblockAccount(client *Gotodon) (MRelationship, error) {
	rel, err := gotodonr.UnblockAccount(client.BaseUrl, client.AccessToken, account.Id)
	return MRelationship{rel}, err
}
