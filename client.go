package gotodon

import (
	"encoding/json"
	"gitlab.com/KopfKrieg/gotodonr"
	"io/ioutil"
	"os"
)

// Set of credentials used to login
type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// A Mastodon client API written in Go
type Gotodon struct {
	BaseUrl     string `json:"base_url"`
	Scope       string `json:"scope"`
	RedirectUri string `json:"redirect_uri"`
	Filepath    string `json:"-"`
	MApp
	MToken
	Login
}

// Creates a new client
func CreateClient(baseUrl, scope, redirectUri, filepath string) Gotodon {
	return Gotodon{
		BaseUrl:     baseUrl,
		Scope:       scope,
		RedirectUri: redirectUri,
		Filepath:    filepath,
	}
}

// Creates a new client with default settings
func CreateDefaultClient(baseUrl, filepath string) Gotodon {
	return CreateClient(baseUrl, gotodonr.Scopes(gotodonr.ScopeRead, gotodonr.ScopeWrite, gotodonr.ScopeFollow), "", filepath)
}

// Loads a client from the given string
func LoadClientFromString(dat string) (error, Gotodon) {
	var gtdn Gotodon
	err := json.Unmarshal([]byte(dat), &gtdn)
	if err != nil {
		return err, Gotodon{}
	}
	return nil, gtdn
}

// Loads a client from the given path
func LoadClient(path string) (error, Gotodon) {
	dat, err := ioutil.ReadFile(path)
	if err != nil {
		return err, Gotodon{}
	}
	err, gtdn := LoadClientFromString(string(dat))
	if err != nil {
		return err, Gotodon{}
	}
	gtdn.Filepath = path
	return nil, gtdn
}

// Loads a client from the given baseUrl and accessToken
//
// However, the client lacks various variables (like ClientId, ClientSecret, ...) which prevents all token-related
// functions (like RevokeToken and all Authorization methods)
func LoadClientFromAccessToken(baseUrl, accessToken string) (error, Gotodon) {
	app, err := gotodonr.VerifyCredentials(baseUrl, accessToken)
	if err != nil {
		return err, Gotodon{}
	}
	return nil, Gotodon{
		BaseUrl: baseUrl,
		MApp: MApp{
			app,
		},
		MToken: MToken{
			gotodonr.Token{
				AccessToken: accessToken,
			},
		},
	}
}

// Serializes the client and returns it as a string
func (client *Gotodon) ToString() (error, string) {
	dat, err := json.Marshal(client)
	if err != nil {
		return err, ""
	}
	return nil, string(dat)
}

// Stores the client at the given path (leave empty to store at client.Filepath)
func (client *Gotodon) Store(path string) error {
	if path == "" {
		path = client.Filepath
	}
	err, dat := client.ToString()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(path, []byte(dat), os.ModePerm)
}

// Checks if the client is registered (i.e. retrieved a ClientId and ClientSecret)
func (client *Gotodon) IsRegistered() bool {
	return client.ClientId != "" && client.ClientSecret != ""
}

// Checks if the client is authorized (i.e. retrieved an AccessToken)
func (client *Gotodon) IsAuthorized() bool {
	return client.Token.AccessToken != ""
}

// Checks if the client can access client-level functions (functions which are not bound to an account)
func (client *Gotodon) HasClientAccess() bool {
	return client.VerifyAccessToken() == nil
}

// Checks if the client can access account-level functions (functions bound to an account).
// Account-Level functions also include client-level functions (so if hasAccountAccess is true, it is safe
// to assume that hasClientAccess is also true).
func (client *Gotodon) HasAccountAccess() (bool, error) {
	res, err := client.VerifyAccountCredentials()
	if err != nil {
		return false, err
	}
	return res.Id != "", nil
}
